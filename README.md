# extractQA - Extraction of Quality Attributes from User Stories

This repository contains the sources for the sub-project dealing with the extraction of Quality attributes from User Stories.

It is part of the umbrella project Machine Learning for Agile Software Development (ML4ASD)