import os 
import pandas as pd
import numpy as np
import ast
import matplotlib.pyplot as plt
import matplotlib

def show_eval(precision,recall,f1_score,name_QA,label_x = "Epoch",path_save_svg=None):
    x = np.arange(0, len(precision))

    fig, ax = plt.subplots()
    ax.plot(x, precision,label="precision")
    ax.plot(x, recall,label="recall")
    ax.plot(x, f1_score,label="f1-score")

    plt.legend(loc=4,borderaxespad=0.5)

    ax.set(xlabel=label_x, ylabel='score',
           title='Evaluation spacy model by '+label_x+' for QA : '+name_QA)
    ax.grid()
    


    #fig.savefig("test.png")
    if(path_save_svg ):
        print("on save ",path_save_svg)
       
        plt.savefig(path_save_svg, format="svg")

    plt.show()
    print("max précision : ",max(precision))
    print("max recall : ",max(recall))
    print("max f1_score : ",max(f1_score))
    array_f1 = np.array(f1_score)
    index=np.argmax(array_f1)    
    print("précision for f1_score : ",precision[index])
    print("recall for f1_score : ",recall[index])
    print("f1_score for f1_score : ",f1_score[index])




    
def show_list_eval(list_QA,df,df2=None,df_path_save_svg=None,df2_path_save_svg=None):
    observations = [ast.literal_eval(lab) for lab in list(df["eval"])] 
    if(df2 is not None): 
        observations2 = [ast.literal_eval(lab) for lab in list(df2["eval"])]
    dic_obs = {}

    for qa in list_QA:
        try :
            dic_obs[qa] = [obs[qa]["eval"] for obs in observations ]
        except KeyError:
            dic_obs[qa] = [obs[qa] for obs in observations ]
        precision = [obs["textcat_p"] for obs in dic_obs[qa]]
        recall = [obs["textcat_r"] for obs in dic_obs[qa]]
        f1_score = [obs["textcat_f"] for obs in dic_obs[qa]]
        show_eval(precision,recall,f1_score,qa,path_save_svg=df_path_save_svg+"_"+qa+".svg")
        
        if(df2 is not None):
            try :
                dic_obs[qa] = [obs[qa]["eval"] for obs in observations2 ]
            except KeyError:
                dic_obs[qa] = [obs[qa] for obs in observations2 ]

            precision = [obs["textcat_p"] for obs in dic_obs[qa]]
            recall = [obs["textcat_r"] for obs in dic_obs[qa]]
            f1_score = [obs["textcat_f"] for obs in dic_obs[qa]]
            show_eval(precision,recall,f1_score,qa,path_save_svg=df2_path_save_svg+"_"+qa+".svg")
        print("----------------------")

def saved_mean_eval_k_fold(path,name_file,list_QA,nb_epoch=100,nb_fold=10):
    list_eval =[]
    for epoch in range(0,nb_epoch):
        dic_eval={}
        for qa in list_QA:
            dic_eval[qa]={
                "textcat_f":0,
                "textcat_r":0,
                "textcat_p":0,
                "eval":{
                    "textcat_f":0,
                    "textcat_r":0,
                    "textcat_p":0,
                }
            }

        list_eval.append(dic_eval)

    for i in range(0,nb_fold):
        file = name_file+"/"+name_file+"_fold_"+str(i)+"/eval_"+name_file+"_fold_"+str(i)+".csv"
        df = pd.read_csv(path+file)
        for epoch in range(0,nb_epoch):
            for qa in list_QA :
                observations = [ast.literal_eval(lab) for lab in list(df["eval"])]
                if("eval_global" == qa):
                    f1 = list_eval[epoch][qa]["textcat_f"] + observations[epoch][qa]["textcat_f"]
                    r = list_eval[epoch][qa]["textcat_r"] + observations[epoch][qa]["textcat_r"]
                    p = list_eval[epoch][qa]["textcat_p"] + observations[epoch][qa]["textcat_p"]


                else:
                    f1 = list_eval[epoch][qa]["textcat_f"] + observations[epoch][qa]["eval"]["textcat_f"]
                    r = list_eval[epoch][qa]["textcat_r"] + observations[epoch][qa]["eval"]["textcat_r"]
                    p = list_eval[epoch][qa]["textcat_p"] + observations[epoch][qa]["eval"]["textcat_p"]

                list_eval[epoch][qa]={
                    "textcat_f":f1,
                    "textcat_r":r,
                    "textcat_p":p,
                    "eval":{}
                }


    for epoch in range(0,nb_epoch):
        for qa in list_QA:
            list_eval[epoch][qa]["eval"]["textcat_f"]=list_eval[epoch][qa]["textcat_f"]/10
            list_eval[epoch][qa]["eval"]["textcat_r"]=list_eval[epoch][qa]["textcat_r"]/10
            list_eval[epoch][qa]["eval"]["textcat_p"]=list_eval[epoch][qa]["textcat_p"]/10

    df_show = pd.DataFrame()
    df_show["eval"] = list_eval
    df_show["eval"]= df_show["eval"].apply(lambda x : str(x))
    df_show.to_csv(path+name_file+"/"+name_file+"df_mean_eval.csv")
    print("Mean Eval Saved : "+path+name_file+"/"+name_file+"df_mean_eval.csv")
    print("end")
    return df_show