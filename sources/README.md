# Sources for extractQA - Extraction of Quality Attributes from User Stories

This folder contains all source code for the extraction of Quality attributes from User Stories.

It is part of the umbrella project Machine Learning for Agile Software Development (ML4ASD)

## Goal
In this project, we aim to prove that it is possible to detect the presence of a quality attribute in a user history and the type of this quality using machine learning. To do this, we used a database composed of 1,675 US dollars distributed among 22 backlogs of industrial or university projects.  This database was created by F. Dalpiaz. [link]
For each user story, Fabian Gilson and Matthias Galster identified the quality attributes present in each user story.
To achieve our objective, we trained different machine learning models (SpaCy, LinearSVC, LogisticRegression and ComplementNB) with different encoding (TF-IDF, Glove and BERT).

## Results

For the detection of the presence of a quality attribute in a user history it is the ComplementNB model with TF-IDF encoding that obtains the best results with a F1-Score Global of 0.71.

For the identification of the types of quality attributes present in a user history it is SpaCy that obtains the best F1-Score Global with 0.53.
 


## How to use

### Install dependency

```
pip install -r requirement.txt
```


### Preprocessing Data 

```
python src/preprocess_data.py -c config/spacy_US_QA.json
```

You can modify the preprocessing by modifying the config file

### Train the model SpaCy
For the other models(No SpaCy), these have been trained in /notebooks/Multi label text classification.ipynb

#### For the detection of the presence of an QA or not

```
python src/spacy_text_class_contain_QA.py -c config/spacy_US_QA.json
```

#### For the identification of QA types

```
python src/spacy_text_class_US_QA.py -c config/spacy_US_QA.json
```

You can modify the input/output and the train by modifying the config file


### Config file 


```json
{
  "data":{
    "data_train_path": "Path the file containing the train data (We use it when there is no cross-validation)",
    "data_test_path":"Path the file containing the test data (We use it when there is no cross-validation)",
    "all_data_path":"Path the file containing the all data (We use it for cross-validation)",
    "dir_data_raw_path":"Path the to folder containing the raw data files",
    "list_QA":"List of QA use in preprocessing (To detect the presence of an AQ, simply indicate {QA})"
  },
  "model_train":{
    "name":"Name Model use for save",
    "save_model_path":"Path the to folder where we save model if nothing is specified, it will not be saved",
    "n_texts":"nb texts",
    "n_epoch":"number of epoch to train the model",
    "path_save_obs":"Path the to folder where we save data dev prediction (US,label,value prediction)",
    "list_label":"List of QA use to train the model (List label for model)",
    "k_fold":"Number of fold to use for cross validation. If the number is equal to 0 it means that we do not do cross validation",
    "ratio":"This value is the decision threshold for deciding whether or not the QA is present. If the predicted value is below this threshold, the QA is not present."

  },

  "preprocessing":{
    "id_data": "Data preprocessing name for the file",
    "size_data_with_no_label":"Size of US with no label (Float in 0 ,1)",
    "test_size": "Size of data test (Float in 0 ,1)"
  }
  
}
```