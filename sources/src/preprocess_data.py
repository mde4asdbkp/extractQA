import os 
import pandas as pd
import numpy as np
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split





from helpers.args import get_args
from helpers.utils import dump_json, load_json, create_dir

def main(config):

    path_data_raw = config["data"]["dir_data_raw_path"]

    list_name_file_data_raw = os.listdir(path_data_raw)

    df_all_raw_data = pd.DataFrame()
    for nf in list_name_file_data_raw:
        print(nf)
        if (len(nf.split("."))>1 and nf.split(".")[1]=="csv"):
            df = pd.read_csv(path_data_raw + nf)        
            check_nan_US = df["User story"] == df["User story"] 
            df = df[check_nan_US].fillna("")
            df["bakclog_id"] = [nf.split(".")[0] for i in range(0,len(df))]
            df_all_raw_data = pd.concat([df_all_raw_data,df])

    df_all_raw_data = df_all_raw_data[["bakclog_id","User story","User-centric?","Contains QA?","QA Name 1","QA Name 2"]]
    df_all_raw_data = df_all_raw_data.fillna("")

    list_QA = config["data"]["list_QA"]
    dic_QA = {}

    for i in range(0,len(list_QA)):
        dic_QA[list_QA[i]] = i

    #Check if there are no error in label
    all_lab_in_data = np.concatenate([df_all_raw_data['QA Name 1'].unique(), df_all_raw_data['QA Name 2'].unique()])
    all_lab_in_data = list(set(all_lab_in_data))
    try:
        all_lab_in_data.remove("")
    except ValueError:
        print()

    check_list_lab = [lab for lab in all_lab_in_data if not(lab in list_QA)]

    if (len(check_list_lab) > 0):
        print("Attention, il y a une erreur de labélisation : un label inexistant à été trouvé :")
        print(check_list_lab)

    list_vec_QA = list(df_all_raw_data[["QA Name 1","QA Name 2"]].apply(lambda x : create_vec_QA(x,list_QA,dic_QA), axis=1))
    df_all_raw_data["label_vec"] = list_vec_QA  

    matrix_vec_QA = np.array(list_vec_QA)

    for i in range(0,len(list_QA)):
        df_all_raw_data[list_QA[i]] = matrix_vec_QA[:,i]


    df_all_raw_data["label_json"] = df_all_raw_data[["label_vec"]].apply(lambda x : create_label_json(x,list_QA), axis=1)
    df_all_raw_data["label_json"] = df_all_raw_data[["label_json"]].apply(lambda x : str(x[0]).replace("'", '"'), axis=1)


    df_all_raw_data["user_centric_bool"] = df_all_raw_data[["User-centric?"]].apply(lambda x : (x == "Yes")  , axis=1)
    df_all_raw_data["contains_QA_bool"] = df_all_raw_data[["Contains QA?"]].apply(lambda x : (x == "Yes")  , axis=1)

    df_all_raw_data["user_centric_val"] = df_all_raw_data[["user_centric_bool"]].apply(convert_bool_val , axis=1)
    df_all_raw_data["contains_QA_val"] = df_all_raw_data[["contains_QA_bool"]].apply(convert_bool_val  , axis=1)

    df_all_raw_data = shuffle(df_all_raw_data)

    train,test = split_train_test_equi_QA(config["data"]["list_QA"],df_all_raw_data,config["preprocessing"]["test_size"],config["preprocessing"]["size_data_with_no_label"])
    
    all_data = pd.concat([train,test])
    all_data = all_data.drop_duplicates(subset=['User story'], keep="first") 
    all_data = shuffle(all_data)


    print("Dataset all : ",len(all_data))
    print("Dataset train : ",len(train))
    print("Dataset test : ",len(test))

    print()
    for qa in list_QA:
        print(qa ," : ",len(all_data[all_data[qa]==1]))
    

    train.to_csv(config["data"]["data_train_path"],index_label=False)
    test.to_csv(config["data"]["data_test_path"],index_label=False)
    all_data.to_csv(config["data"]["all_data_path"],index_label=False)



def create_vec_QA(data,list_QA,dic_QA):
    vec_QA = np.zeros(len(list_QA))
    for d in data :
        if d != "" :
            vec_QA[dic_QA[d]] = 1
    return vec_QA

def create_label_json(vec_QA,list_QA):
    json_lab = {}
    for i in range(0,len(list_QA)):
        json_lab[list_QA[i]] = vec_QA[0][i] #(vec_QA[0][i] == 1)
    return json_lab  

def convert_bool_val(b):
    if(b.bool()): return 1 
    else: return 0

def split_train_test_equi_QA(list_QA,df,test_size=0.33,no_label_size=0.33):
    """
    """
    train_df = pd.DataFrame()
    test_df = pd.DataFrame()
    for qa in list_QA:
        df_QA = df[df[qa]==1]
        X_train, X_test = train_test_split(df_QA,test_size=test_size, random_state=42)

        train_df = pd.concat([train_df,X_train])
        test_df = pd.concat([test_df,X_test])
        
    df_no_lab  = df[ df["contains_QA_bool"]!=True]
    size_no_lab = int(len(df_no_lab)*no_label_size)

    X_train, X_test = train_test_split(df_no_lab[:size_no_lab],test_size=test_size, random_state=42)
    train_df = pd.concat([train_df,X_train])
    test_df = pd.concat([test_df,X_test])

     #Remove test_data found in train_data 
    test_df = test_df[~test_df["User story"].isin(train_df["User story"]) ]
    return train_df , test_df

 


if __name__ == '__main__':
    args = get_args()
    config = load_json(args.config)
    main(config)

