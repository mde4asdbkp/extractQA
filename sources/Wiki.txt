Our objective is to create a model that can tell if a user story has an AQ or not.


Let's analyze the data. In our case, we have sentences (US) and for each of them we have between 0 and 2 QA. 

The number of US per QA :

performance : 32
compatibility : 204
usability : 204
reliability : 22
security : 70
maintainability : 61
portability : 37

Nb Total US : 1469
Nb US with QA : 566
Nb US user centric : 1205

For this, we will use Spacy, one of the most powerful and fastest NLP library based on Deeplearning. 

We used the pre-trained en_core_web_lg model (Because in a case like ours with little data, it is strongly recommended to use a pre-trained model)
English multi-task CNN trained on OntoNotes, with GloVe vectors trained on Common Crawl. Assigns word vectors, context-specific token vectors, POS tags, dependency parse and named entities.

To create a multi-label classifier in Spacy, we just need to load a space model and add our labels. 
After that, we will train our 100 epochs model to see when it reaches its learning peak. 
To improve the efficiency of the training will only use minibatchsize (for each epoch, we will cut the train dataset into small dataset (between 4 and 32 elements, the further away we are in the epochs the higher the number of elements is, this is to allow the model to explore as much as possible on the first epoch and to focus towards its optimum on the end))



evaluation metric :

    precision = tp / (tp + fp)
    Number of good positive predictions on the total number of predictions.

    recall = tp / (tp + fn)
    Number of good documents predicted positive on all positive documents

    f_score = 2 * (precision * recall) / (precision + recall)
    Good measure to combine recall and precision

We use these 3 metrics because our objective is to maximize the number of predictions while minimizing bad predictions.

Meta parameter to explore  : 

Nb epoch : TODO 
Size DataTrain : TODO to show the importance to label more data
Embedding : Try other Embedding (Bert new state of art - beta test in spacy)

TODO : 
Explore prediction errors to better understand problems. Outliner? Special case not in train data?
Explore other lib : Flair : other power full to create pipeline for NLP Model.

 





